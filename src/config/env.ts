import * as dotenv from 'dotenv'
dotenv.config({ path: '.env' })
const urls = require(`./${process.env.ENV}/baseUrl.conf`)

module.exports = {
    urls
}
