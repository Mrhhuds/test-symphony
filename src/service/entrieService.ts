import chai = require("chai");
import chaiHttp = require("chai-http");

chai.use(chaiHttp)

const config = require('../config/env')

export async function getEntries () {
    return await chai
    .request(config.urls.baseUrl)
    .get('/entries')
}
