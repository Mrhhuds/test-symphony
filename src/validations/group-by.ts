import chai = require('chai')

const expect = chai.expect

export function filterByEntries (obj: any, expected: any, value: number) {
    const entries = obj.body.entries.filter(entry => entry.Category === expected)
    const filteredEntries = entries.length
    expect(filteredEntries == value)
    console.log(`it was found ${filteredEntries} entries`)
    console.log(entries)
}