/* eslint-disable no-unused-expressions */
import chai = require('chai')
const expect = chai.expect

export function checkArrayIsNotEmpty (array: Array<any>) {
    expect(array).not.to.be.empty
}

export function checkJsonIsNotEmpty (json: JSON) {
    expect(json).not.to.be.empty
}
