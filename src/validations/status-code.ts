import chai = require('chai')
import * as fs from 'fs'
const expect = chai.expect

require('request-to-curl')

export function checkStatusCode (obj, code) {
    if (obj.statusCode !== code) {
        createErrorFile(obj)
    }
    return expect(obj).to.have.status(code)
}

export function checkEqual (objA, objB) {
    const result = expect(objA.body).to.deep.equal(objB.body)
    if (!result) {
        console.log(objA.body)
        console.log(objB.body)
    }
    return result
}

function createErrorFile (obj) {
    if (!fs.existsSync('error-files')) {
        fs.mkdirSync('error-files')
    }
}
