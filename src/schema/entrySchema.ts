const Joi = require('joi')
    .extend(require('@joi/date'))

export const entriesSchema = Joi.object(
    {
        count: Joi.number(),
        entries: Joi.array().items(Joi.object(
           {
                API: Joi.string(),
                Description: Joi.string(),
                Auth: Joi.string(). allow(""),
                HTTPS: Joi.boolean(),
                Cors: Joi.string(),
                Link: Joi.string(),
                Category: Joi.string()
           }
        ))
    }
)