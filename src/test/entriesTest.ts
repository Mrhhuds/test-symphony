describe('Get entries', async () => {

	const entriesService = require('../service/entrieService')
	const validation = require('../validations/group-by')
	const statusCode = require('../validations/status-code')
	const schema = require('../schema/entrySchema')
	const schemaValidation = require('../validations/schema')
	const checkempty = require('../validations/empty')

	context('Get Entries', async () => {
		it('Get Entries by category \n\t /entries', async () => {
			const getEntries = await entriesService.getEntries()
			statusCode.checkStatusCode(getEntries, 200)
			checkempty.checkJsonIsNotEmpty(getEntries)
	 		schemaValidation.validateSchema(getEntries.body, schema.entriesSchema)
			validation.filterByEntries(getEntries, 'Authentication & Authorization', 7)
		})
	})
})
