# API MONITOR - Symphony Solutions test task test

## 📖 Overview:

The API monitor is a repository created to do the test task test by Hudson Brito

Mainly libraries used:

NodeJS,
Typescript,
Chai,
Mocha

## 🚀 Getting Started:

### Prerequisites:

- Node.js v14.16.1
- npm 6.14

## Install dependencies
```shell
    $ npm install
```
## Create .env file

Create file in root folder, named as '.env' and with the following content
```shell
BASE_URL=https://api.publicapis.org
```

## Run the tests suit on local machine:

```shell
    $ npm run test:local
```
## reports

see the result on console, and if running locally, To see the generated report open /mochawesome-report/mochawesome.html cath the file and open in your browser 

## Run test suit on Gitlab CI/CD

Open the repository page, go to Build -> Pipeline -> Run Pipeline -> select branch: Main -> run pipelne
